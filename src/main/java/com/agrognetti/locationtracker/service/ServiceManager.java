package com.agrognetti.locationtracker.service;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.core.content.ContextCompat;

public abstract class ServiceManager {

    private static final String TAG = ServiceManager.class.getName();

    public static void runForegroundServices(Context context) {
        Intent serviceIntent = new Intent(context, SyncService.class);
        ContextCompat.startForegroundService(context, serviceIntent);
        Log.d(TAG, "SyncService was started");
    }

    public static void stopForegroundServices(Context context) {
        Intent serviceIntent = new Intent(context, SyncService.class);
        context.stopService(serviceIntent);
        Log.d(TAG, "SyncService was stopped");
    }
}
