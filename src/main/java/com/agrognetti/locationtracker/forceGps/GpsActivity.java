package com.agrognetti.locationtracker.forceGps;

import android.content.IntentFilter;

import com.agrognetti.locationtracker.permission.PermissionsActivity;

public class GpsActivity extends PermissionsActivity implements GpsInterface {

    private GpsReceiver gpsReceiver;


    @Override
    protected void onStart() {
        super.onStart();

        IntentFilter mfilter = new IntentFilter("android.location.PROVIDERS_CHANGED");
        gpsReceiver = new GpsReceiver(this);
        registerReceiver(gpsReceiver, mfilter);
    }

    @Override
    protected void onResume() {
        GpsUtil.checkGPSEnabled(getApplicationContext(), this);
        super.onResume();
    }

    @Override
    protected void onStop() {
        unregisterReceiver(gpsReceiver);
        super.onStop();
    }

    @Override
    public void onGpsStatusChanged() {
        GpsUtil.checkGPSEnabled(getApplicationContext(), this);
    }

}
