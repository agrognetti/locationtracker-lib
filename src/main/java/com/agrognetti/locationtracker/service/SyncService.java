package com.agrognetti.locationtracker.service;

import android.app.Notification;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.IBinder;

import androidx.annotation.Nullable;

import com.agrognetti.locationtracker.forceGps.GpsReceiver;
import com.agrognetti.locationtracker.location.LocationProvider;
import com.agrognetti.locationtracker.notification.NotificationsUtil;

import java.util.concurrent.Callable;

import static com.agrognetti.locationtracker.notification.NotificationsUtil.ENABLE_GPS_NOTIFICATION_ID;

public class SyncService extends Service {

    private static final int FOREGROUND_SERVICE_SYNC_ID = 5;
    private static long intervalExecutionService = 150000; // Default interval - 2.5 minutes
    private static long delayBeforeFirstExecution = 0; // Default interval - 2.5 minutes
    private static Callable tasks;
    private Context context;
    private GpsReceiver gpsReceiver;
    private Handler handler;

    public static void setTasks(Callable tasks) {
        SyncService.tasks = tasks;
    }

    public static void setIntervalExecutionService(long intervalExecutionService) {
        SyncService.intervalExecutionService = intervalExecutionService;
    }

    public static void setDelayBeforeFirstExecution(long delayBeforeFirstExecution) {
        SyncService.delayBeforeFirstExecution = delayBeforeFirstExecution;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();

        //A listener is created to show or delete the GPS notification warning during the ForegroundService life.
        IntentFilter mfilter = new IntentFilter("android.location.PROVIDERS_CHANGED");
        gpsReceiver = new GpsReceiver(() -> {
            if (!LocationProvider.isProviderEnabled(context)) {
                NotificationsUtil.showEnableGpsNotification(context);
            } else {
                NotificationsUtil.deleteNotification(context, ENABLE_GPS_NOTIFICATION_ID);
            }
        });

        registerReceiver(gpsReceiver, mfilter);
    }

    @Override
    public void onDestroy() {
        //Stop location tracker updates
        LocationProvider.getInstance().removeLocationUpdates();
        //Removes all tasks queued
        handler.removeCallbacksAndMessages(null);
        //Unregister GPS Listener
        unregisterReceiver(gpsReceiver);
        //Delete GPS notification in case it's still showing it
        NotificationsUtil.deleteNotification(context, ENABLE_GPS_NOTIFICATION_ID);
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Notification notification = NotificationsUtil.createForegroundSyncServiceNotification(context);
        startForeground(FOREGROUND_SERVICE_SYNC_ID, notification);

        if (handler != null) {
            //Removes all tasks queued -> Avoids to have two instances of runnables (Scheduled tasks) in case onStartCommand is called twice
            handler.removeCallbacksAndMessages(null);
        }

        //Do heavy work on a background thread
        startTasks();

        return START_NOT_STICKY;
    }

    public void startTasks() {
        // Starts to request location, this is done only once
        LocationProvider.getInstance().runLocationTracker(context);

        // Create the Handler object (on the main thread by default)
        handler = new Handler();

        // Define the code block to be executed
        Runnable runnableCode = new Runnable() {
            @Override
            public void run() {
                // Do something here on the main thread

                if (tasks != null) {
                    try {
                        tasks.call();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                // Repeat this the same runnable code block again in a INTERVAL seconds. 'this' is referencing the Runnable object
                handler.postDelayed(this, intervalExecutionService);
            }
        };
        // Start the initial runnable task by posting through the handler
        handler.postDelayed(runnableCode, delayBeforeFirstExecution);
    }
}
