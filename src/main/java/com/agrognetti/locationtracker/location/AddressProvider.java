package com.agrognetti.locationtracker.location;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

import com.agrognetti.locationtracker.util.ResultListener;

public class AddressProvider {

    public static void requestCurrentAddress(final Context context, final ResultListener<Address> resultListener) {
        LocationProvider.getInstance().requestCurrentLocation(context, location -> {
            if (location != null) {
                Geocoder geocoder = new Geocoder(context);
                try {
                    Address currentAddress = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1).get(0);
                    resultListener.finish(currentAddress);
                } catch (Exception e) {
                    resultListener.finish(null);
                }
            } else {
                resultListener.finish(null);
            }
        });
    }
}
