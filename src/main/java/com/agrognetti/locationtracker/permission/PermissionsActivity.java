package com.agrognetti.locationtracker.permission;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.agrognetti.locationtracker.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import java.util.ArrayList;
import java.util.List;


public class PermissionsActivity extends AppCompatActivity {

    public static final int REQUEST_MULTIPLE_PERMISSIONS = 1;
    private final int REQUEST_PERMISSIONS_DENIED = 2;
    private List<String> listPermissionsNotGranted;
    private List<String> listPermissionsNeeded = new ArrayList<>();

    protected void addPermissionNeeded(String permission) {
        listPermissionsNeeded.add(permission);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!areAllPermissionsGranted() && android.os.Build.VERSION.SDK_INT >= 23) {
            requestPermissions();
        } else {
            isGooglePlayServicesAvailable();
        }
    }

    protected boolean areAllPermissionsGranted() {
        listPermissionsNotGranted = new ArrayList<>();
        for (String permission : listPermissionsNeeded) {
            if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNotGranted.add(permission);
            }
        }

        return listPermissionsNotGranted.isEmpty();
    }

    protected void requestPermissions() {
        ActivityCompat.requestPermissions(this, listPermissionsNotGranted.toArray(new String[listPermissionsNotGranted.size()]), REQUEST_MULTIPLE_PERMISSIONS);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        boolean showPermissionsRationale = true;
        List<String> listPermissionsNotGranted = new ArrayList<>();

        if (requestCode == REQUEST_MULTIPLE_PERMISSIONS) {
            for (int i = 0, len = permissions.length; i < len; i++) {
                String permission = permissions[i];
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    // user rejected the permission
                    if (android.os.Build.VERSION.SDK_INT >= 23 && !shouldShowRequestPermissionRationale(permission)) {
                        // User reject the permission and also CHECKED "never ask again".
                        showPermissionsRationale = false;
                    } else if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                        // User reject permission but did NOT check "never ask again".
                        listPermissionsNotGranted.add(permission);
                    }
                }
            }

            if (!listPermissionsNotGranted.isEmpty()) {
                ActivityCompat.requestPermissions(this, listPermissionsNotGranted.toArray(new String[listPermissionsNotGranted.size()]), REQUEST_MULTIPLE_PERMISSIONS);
            } else if (!showPermissionsRationale) {
                forceToAllowPermissions();
            }
        }

        if (areAllPermissionsGranted()) {
            isGooglePlayServicesAvailable();
        }
    }

    public void forceToAllowPermissions() {
        showDialogOK(getString(R.string.accept_all_permissions),
                (dialog, which) -> {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            Intent intent = new Intent();
                            intent.setAction(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getPackageName(), null);
                            intent.setData(uri);
                            startActivityForResult(intent, REQUEST_PERMISSIONS_DENIED);
                            break;
                        case DialogInterface.BUTTON_NEGATIVE:
                            // Proceed with logic by disabling the related features or quit the app.
                            finish();
                            break;
                    }
                });
    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.go_to_permissions), okListener)
                .setNegativeButton(getString(R.string.exit_app), okListener)
                .create()
                .show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_PERMISSIONS_DENIED || !isGooglePlayServicesAvailable()) {
            recreate();
        }
    }

    protected boolean isGooglePlayServicesAvailable() {
        int googlePlayStatus = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this);
        int MISSING_PLAY_SERVICES = 1000;

        if (googlePlayStatus != ConnectionResult.SUCCESS) {
            Dialog dialog = GoogleApiAvailability.getInstance().getErrorDialog(this, googlePlayStatus, MISSING_PLAY_SERVICES);
            dialog.setCancelable(false);
            dialog.show();

            dialog.setOnDismissListener(dialog1 -> isGooglePlayServicesAvailable());

            return false;
        } else {
            return true;
        }
    }
}
