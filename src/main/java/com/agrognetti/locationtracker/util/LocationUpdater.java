package com.agrognetti.locationtracker.util;

import android.location.Location;

public interface LocationUpdater {

    void onLocationUpdate(Location location);
}
