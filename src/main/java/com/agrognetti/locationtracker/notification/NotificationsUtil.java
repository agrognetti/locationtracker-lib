package com.agrognetti.locationtracker.notification;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.Settings;

import androidx.core.app.NotificationCompat;

import com.agrognetti.locationtracker.R;

import static android.content.Context.NOTIFICATION_SERVICE;

public class NotificationsUtil {

    public static final int ENABLE_GPS_NOTIFICATION_ID = 0;
    private static final String CHANNEL_FOREGROUND_SERVICE_SYNC_ID = "channel_foreground_service_sync";

    private static int appIcon = R.drawable.logo_small;


    private static NotificationChannel createChannel(NotificationManager notificationManager, String channelId, String channelName, int importance) {
        NotificationChannel notificationChannel = null;

        if (notificationManager != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                notificationChannel = notificationManager.getNotificationChannel(channelId);

                if (notificationChannel == null) {
                    notificationChannel = new NotificationChannel(channelId, channelName, importance);

                    //Set "showBadge" in false to avoid show indicator in the app icon when the foreground service notification is showing.
                    if (channelId.equals(CHANNEL_FOREGROUND_SERVICE_SYNC_ID)) {
                        notificationChannel.setShowBadge(false);
                    }

                    notificationManager.createNotificationChannel(notificationChannel);
                }
            }
        }

        return notificationChannel;
    }

    private static void showNotification(NotificationManager notificationManager, int notificationId, Notification notification) {
        if (notificationManager != null) {
            notificationManager.notify(notificationId, notification);
        }
    }

    public static void deleteNotification(Context context, int notificationId) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.cancel(notificationId);
        }
    }

    public static Notification createForegroundSyncServiceNotification(Context context) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        createChannel(notificationManager, CHANNEL_FOREGROUND_SERVICE_SYNC_ID, context.getString(R.string.channel_foreground_service_sync), NotificationManager.IMPORTANCE_LOW);

        return new NotificationCompat.Builder(context, CHANNEL_FOREGROUND_SERVICE_SYNC_ID)
                .setContentTitle(context.getString(R.string.app_name))
                .setSmallIcon(appIcon)
                .build();
    }

    public static void showEnableGpsNotification(Context context) {
        NotificationCompat.Builder notifBuilder = new NotificationCompat.Builder(context, CHANNEL_FOREGROUND_SERVICE_SYNC_ID)
                .setSmallIcon(appIcon)
                .setAutoCancel(false)
                .setOngoing(true)
                .setContentTitle(context.getString(R.string.notif_title_enable_gps))
                .setContentText(context.getString(R.string.notif_message_enable_gps));

        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);

        PendingIntent resultPendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notifBuilder.setContentIntent(resultPendingIntent);

        Notification notification = notifBuilder.build();
        notification.defaults |= Notification.DEFAULT_SOUND;
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        notification.defaults |= Notification.DEFAULT_LIGHTS;

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        createChannel(notificationManager, CHANNEL_FOREGROUND_SERVICE_SYNC_ID, context.getString(R.string.channel_foreground_service_sync), NotificationManager.IMPORTANCE_LOW);
        showNotification(notificationManager, ENABLE_GPS_NOTIFICATION_ID, notification);
    }

    public static void setAppIcon(int appIconIntResource) {
        appIcon = appIconIntResource;
    }

}
