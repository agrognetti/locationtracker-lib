package com.agrognetti.locationtracker.forceGps;

import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.provider.Settings;

import com.agrognetti.locationtracker.location.LocationProvider;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.Task;

public class GpsUtil {

    public static void checkGPSEnabled(final Context context, Activity activity) {
        if (!isLocationEnabled(context)) {
            enableLocation(context, activity);
        }
    }

    private static boolean isLocationEnabled(Context context) {
        boolean result = false;

        try {
            int locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            if (locationMode == Settings.Secure.LOCATION_MODE_HIGH_ACCURACY) {
                result = true;
            }
        } catch (Settings.SettingNotFoundException e) {

        }

        return result;
    }

    // Enable location with dialog like GoogleMaps
    private static void enableLocation(final Context context, final Activity activity) {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .build();

        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(LocationProvider.getInstance().getLocationRequestInterval());
        locationRequest.setFastestInterval(LocationProvider.getInstance().getLocationRequestFastestInterval());

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);

        //************************
        builder.setAlwaysShow(true); // This is the key ingredient
        //************************

        Task<LocationSettingsResponse> result = LocationServices.getSettingsClient(context).checkLocationSettings(builder.build());

        result.addOnCompleteListener(task -> {
            try {
                task.getResult(ApiException.class);
                // All location settings are satisfied. The client can initialize location requests here.
            } catch (ApiException exception) {
                switch (exception.getStatusCode()) {
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user a dialog.
                        try {
                            // Cast to a resolvable exception.
                            ResolvableApiException resolvable = (ResolvableApiException) exception;
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            resolvable.startResolutionForResult(activity, 1000);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.

                        } catch (ClassCastException e) {
                            // Ignore, should be an impossible error.

                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.

                        break;
                }
            }
        });
    }

}
