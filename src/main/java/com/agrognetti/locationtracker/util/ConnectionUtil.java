package com.agrognetti.locationtracker.util;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;

public abstract class ConnectionUtil {

    public static boolean isAirplaneModeOn(Context context) {
        boolean isAirplaneMode;

        if (Build.VERSION.SDK_INT < 17) {
            isAirplaneMode = Settings.System.getInt(context.getContentResolver(), Settings.System.AIRPLANE_MODE_ON, 0) != 0;
        } else {
            isAirplaneMode = Settings.Global.getInt(context.getContentResolver(), Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
        }

        return isAirplaneMode;
    }
}
