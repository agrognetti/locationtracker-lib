package com.agrognetti.locationtracker.location;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.SystemClock;
import android.util.Log;

import androidx.core.app.ActivityCompat;

import com.agrognetti.locationtracker.util.ConnectionUtil;
import com.agrognetti.locationtracker.util.LocationUpdater;
import com.agrognetti.locationtracker.util.ResultListener;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.List;

public class LocationProvider {

    private static final String TAG = LocationProvider.class.getName();
    private static final int MAX_REQUEST_LOCATION_UPDATES = 10;

    private static LocationProvider instance;

    private FusedLocationProviderClient fusedLocationClient;
    private LocationCallback locationCallback;
    private KalmanLatLong kalmanFilter;

    private List<LocationUpdater> locationUpdaters;

    private static int locationNumUpdates;
    private long runStartTimeInMillis;
    private int locationRequestInterval = 5000;
    private int locationRequestFastestInterval = 5000;
    private float smallestDisplacement = 25.0f;
    private float maximumAccuracy = 25.0f;
    private float maxPredictedDeltaInMeters = 20.0f;
    private float predictedDeltaInMeters = -1.0f;


    private LocationProvider() {
    }

    public static LocationProvider getInstance() {
        if (instance == null) {
            instance = new LocationProvider();
        }

        return instance;
    }

    public static boolean isProviderEnabled(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return locationManager != null && (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER));
    }

    public void addLocationUpdater(LocationUpdater locationUpdater) {
        if (locationUpdaters == null) {
            locationUpdaters = new ArrayList<>();
        }

        locationUpdaters.add(locationUpdater);
    }

    public float getSmallestDisplacement() {
        return smallestDisplacement;
    }

    public void setSmallestDisplacement(float smallestDisplacement) {
        this.smallestDisplacement = smallestDisplacement;
    }

    public float getMaximumAccuracy() {
        return maximumAccuracy;
    }

    public void setMaximumAccuracy(float maximumAccuracy) {
        this.maximumAccuracy = maximumAccuracy;
    }

    public int getLocationRequestInterval() {
        return locationRequestInterval;
    }

    public void setLocationRequestInterval(int locationRequestInterval) {
        this.locationRequestInterval = locationRequestInterval;
    }

    public int getLocationRequestFastestInterval() {
        return locationRequestFastestInterval;
    }

    public void setLocationRequestFastestInterval(int locationRequestFastestInterval) {
        this.locationRequestFastestInterval = locationRequestFastestInterval;
    }

    public float getMaxPredictedDeltaInMeters() {
        return maxPredictedDeltaInMeters;
    }

    public void setMaxPredictedDeltaInMeters(float maxPredictedDeltaInMeters) {
        this.maxPredictedDeltaInMeters = maxPredictedDeltaInMeters;
    }

    public float getPredictedDeltaInMeters() {
        return predictedDeltaInMeters;
    }


    private LocationRequest createLocationRequest() {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(locationRequestInterval);
        locationRequest.setFastestInterval(locationRequestFastestInterval);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setSmallestDisplacement(smallestDisplacement);

        return locationRequest;
    }

    public void runLocationTracker(final Context context) {
        removeLocationUpdates();

        runStartTimeInMillis = SystemClock.elapsedRealtimeNanos() / 1000000;
        kalmanFilter = new KalmanLatLong(3);

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && isProviderEnabled(context)) {

            fusedLocationClient = LocationServices.getFusedLocationProviderClient(context);

            LocationRequest locationRequest = createLocationRequest();

            locationCallback = new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    Location location = locationResult.getLastLocation();

                    if (location != null) {
                        filterLocation(location, context, true);
                    }
                }

            };

            fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
        }
    }

    public void removeLocationUpdates() {
        if (fusedLocationClient != null && locationCallback != null) {
            fusedLocationClient.removeLocationUpdates(locationCallback);
        }
    }

    private boolean filterLocation(Location location, Context context, Boolean notifyLocation) {

        long age = getLocationAge(location);
        float horizontalAccuracy = location.getAccuracy();

        if (age > 5 * 1000) { // more than 5 seconds
            Log.d(TAG, "Location is old");
            return false;
        }

        if (horizontalAccuracy <= 0) {
            Log.d(TAG, "Latitidue and longitude values are invalid.");
            return false;
        }

        if (horizontalAccuracy > maximumAccuracy) { // 25 meter filter
            Log.d(TAG, "Accuracy is too low.");
            return false;
        }

        /* Kalman Filter */
        float Qvalue;

        long locationTimeInMillis = location.getElapsedRealtimeNanos() / 1000000;
        long elapsedTimeInMillis = locationTimeInMillis - runStartTimeInMillis;

        float currentSpeed = location.getSpeed();

        if (currentSpeed == 0.0f) {
            Qvalue = 3.0f; // 3 meters per second
        } else {
            Qvalue = currentSpeed; // meters per second
        }

        kalmanFilter.Process(location.getLatitude(), location.getLongitude(), location.getAccuracy(), elapsedTimeInMillis, Qvalue);
        double predictedLat = kalmanFilter.get_lat();
        double predictedLng = kalmanFilter.get_lng();

        Location predictedLocation = new Location(""); // Provider name is unecessary
        predictedLocation.setLatitude(predictedLat);
        predictedLocation.setLongitude(predictedLng);
//        float predictedDeltaInMeters = predictedLocation.distanceTo(location);
        predictedDeltaInMeters = predictedLocation.distanceTo(location);

        if (predictedDeltaInMeters > maxPredictedDeltaInMeters) {
            Log.d(TAG, "Kalman Filter detects mal GPS, we should probably remove this from track");
            kalmanFilter.consecutiveRejectCount += 1;

            if (kalmanFilter.consecutiveRejectCount > 5) {
                kalmanFilter = new KalmanLatLong(3); // Reset Kalman Filter if it rejects more than 3 times in raw.
            }

            return false;
        } else {
            kalmanFilter.consecutiveRejectCount = 0;
        }

        /* Notify predicted location to UI */
        if (notifyLocation) {
            for (LocationUpdater locationUpdater : locationUpdaters) {
                locationUpdater.onLocationUpdate(location);
            }
        }

        return true;
    }

    private long getLocationAge(Location newLocation) {
        long locationAge;

        if (android.os.Build.VERSION.SDK_INT >= 17) {
            long currentTimeInMilli = (SystemClock.elapsedRealtimeNanos() / 1000000);
            long locationTimeInMilli = (newLocation.getElapsedRealtimeNanos() / 1000000);
            locationAge = currentTimeInMilli - locationTimeInMilli;
        } else {
            locationAge = System.currentTimeMillis() - newLocation.getTime();
        }

        return locationAge;
    }


    public void requestCurrentLocation(final Context context, final ResultListener<Location> resultListener) {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && isProviderEnabled(context) && !ConnectionUtil.isAirplaneModeOn(context)) {

            final FusedLocationProviderClient fusedLocationClient = LocationServices.getFusedLocationProviderClient(context);

            locationNumUpdates = 0;
            LocationRequest locationRequest = createLocationRequest()
                    .setInterval(3000)
                    .setFastestInterval(3000)
                    .setNumUpdates(MAX_REQUEST_LOCATION_UPDATES)
                    .setSmallestDisplacement(0.0f);

            fusedLocationClient.requestLocationUpdates(locationRequest, new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {

                    locationNumUpdates++;
                    Location location = locationResult.getLastLocation();

                    if (location != null) {
                        if (location.getAccuracy() > 0 && location.getAccuracy() < 75) {
                            fusedLocationClient.removeLocationUpdates(this);
                            resultListener.finish(location);

                        } else if (locationNumUpdates == MAX_REQUEST_LOCATION_UPDATES) {
                            resultListener.finish(null);
                        }

                    } else {
                        fusedLocationClient.removeLocationUpdates(this);
                        resultListener.finish(null);
                    }
                }

            }, null).addOnFailureListener(e -> resultListener.finish(null));

        } else {
            resultListener.finish(null);
        }
    }


}
