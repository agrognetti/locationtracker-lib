package com.agrognetti.locationtracker.util;

public interface ResultListener<T> {
    void finish(T result);
}
