package com.agrognetti.locationtracker.forceGps;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class GpsReceiver extends BroadcastReceiver {

    private GpsInterface gpsInterface;

    public GpsReceiver(GpsInterface gpsInterface) {
        this.gpsInterface = gpsInterface;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        gpsInterface.onGpsStatusChanged();
    }

}
