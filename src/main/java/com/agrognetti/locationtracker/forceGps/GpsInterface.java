package com.agrognetti.locationtracker.forceGps;

public interface GpsInterface {
    void onGpsStatusChanged();
}
